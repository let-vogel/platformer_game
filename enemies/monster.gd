extends KinematicBody2D

const TYPE = "enemy"

const GRAVITY = 20
const SPEED = 50
const FLOOR = Vector2(0, -1)

var velocity = Vector2()
var direction = 1

var health = 5

func _physics_process(delta):
	velocity.x = SPEED * direction
	velocity.y += GRAVITY
	velocity = move_and_slide(velocity, FLOOR)
	if health <= 0:
		queue_free()
	if is_on_wall() || !$RayCast2D.is_colliding():
		direction *= -1
		$Sprite.flip_h = !$Sprite.flip_h
		$RayCast2D.position.x *= -1
		
func take_damage(dmg):
	health -= dmg
