extends Area2D

func _on_death_barrier_body_entered(body):
	if body.name == "player" || body.name == "monster":
		body.queue_free()
