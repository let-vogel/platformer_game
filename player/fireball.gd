extends Area2D

const TYPE = "projectile"
const SPEED = 300
var velocity = Vector2() 

var direction = 1

func set_fireball_direction(dir):
	direction = dir
	if dir == -1:
		$AnimatedSprite.flip_h = true
	else:
		$AnimatedSprite.flip_h = false

func _process(delta):
	velocity.x = SPEED * delta * direction
	translate(velocity)
	$AnimatedSprite.play("shoot")

func _on_fireball_body_entered(body):
	if body.name == "monster":
		body.take_damage(1)
		queue_free()
	elif body.name != "fireball" && body.name != "player":
		queue_free()

