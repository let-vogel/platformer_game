extends KinematicBody2D

const TYPE = "player"
const UP = Vector2(0, -1)
const GRAVITY = 20
const MAX_SPEED = 250
const ACCELERATION = 50
const JUMP_HEIGHT = -480
var motion = Vector2()
var jumped_once = false

const FIREBALL = preload("./fireball.tscn")

func _physics_process(delta):
	var friction = false
	motion.y += GRAVITY
	
	# shoot
	if Input.is_action_just_pressed("ui_focus_next"):
		var fireball = FIREBALL.instance()
		fireball.position = $Position2D.global_position
		fireball.set_fireball_direction(sign($Position2D.position.x))
		get_parent().add_child(fireball)
		
	# go right
	if Input.is_action_pressed("ui_right"):
		motion.x = min(motion.x+ACCELERATION, MAX_SPEED)
		$AnimatedSprite.flip_h = false
		$AnimatedSprite.play("Run")
		if sign($Position2D.position.x) == -1:
			$Position2D.position.x *= -1
	
	# go left
	elif Input.is_action_pressed("ui_left"):
		motion.x = max(motion.x-ACCELERATION, -MAX_SPEED)
		$AnimatedSprite.flip_h = true
		$AnimatedSprite.play("Run")
		if sign($Position2D.position.x) == 1:
			$Position2D.position.x *= -1
	else:
		$AnimatedSprite.play("Idle")
		friction = true
		
	if is_on_floor() && !is_on_wall():
		jumped_once = false
		if Input.is_action_just_pressed("ui_up"):
			motion.y = JUMP_HEIGHT
		if friction == true:
			motion.x = lerp(motion.x, 0, 0.2)
	else:
		if Input.is_action_just_pressed("ui_up") && jumped_once == false && is_on_wall() == false:
			motion.y = JUMP_HEIGHT
			jumped_once = true
		if friction == true:
			motion.x = lerp(motion.x, 0, 0.05)

		if motion.y < 0:
			$AnimatedSprite.play("Jump")
		else:
			$AnimatedSprite.play("Fall")
			
	if is_on_wall():
		motion.y = lerp(motion.y, 0, 0.3)
		$AnimatedSprite.play("Idle")
		if Input.is_action_just_pressed("ui_up"):
			motion.y = JUMP_HEIGHT
			if $AnimatedSprite.flip_h == true:
				motion.x = 500
			else:
				motion.x = -500
			
	

	motion = move_and_slide(motion, UP)
